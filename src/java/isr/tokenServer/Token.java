/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.tokenServer;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class Token {
    final private String token;
    final private String id;
    
    public Token(String token, String id) {
        this.token = token;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getToken() {
        return token;
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder(3, 31)
                .append(token)
                .append(id)
                .toHashCode();
    }
    
    @Override 
    public boolean equals(Object obj) {
        if (!(obj instanceof Token))
            return false;
        if (obj == this)
            return true;
        
        Token t = (Token) obj;
        return new EqualsBuilder()
                .append(token, t.token)
                .append(id, t.id)
                .isEquals();     
    }
    
}
