/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.tokenServer;

import datacore.Vehicle;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.HashSet;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class TokensHolder {
    private static TokensHolder holder;
    private static TokenGenerator tokenGenerator;
    //private CreditService
    
    final private HashSet<Token> set;
    
    private TokensHolder() {
        set = new HashSet<>();
    }
    
    public static TokensHolder Init() {
        if(holder == null) {
            synchronized (TokensHolder.class) {
                holder = new TokensHolder();
                tokenGenerator = new TokenGenerator();
            }
        }
        return holder;
    }

    private static class TokenGenerator {
        private final SecureRandom random = new SecureRandom();

        public String nextToken() {
            return new BigInteger(160, random).toString(32);
        }
    }
    
    public boolean isTokenExist(String requestNumber, Vehicle vehicle) {
        Token token = get(requestNumber, vehicle);
        return token != null;
    }
    
    public String getToken(String requestNumber, Vehicle vehicle) {      
        Token token = get(requestNumber, vehicle);
        
        if(token == null) {
            String tokenString = tokenGenerator.nextToken();
            set.add(new Token(tokenString, vehicle.getVid() + requestNumber));
            return tokenString;
        } else {
            return token.getToken();
        }   
    }
    
    public void remove(String requestNumber, Vehicle vehicle) {
        Token token = get(requestNumber, vehicle);
        if(token != null) {
            set.remove(token);
        }
    }
    
    private Token get(String requestNumber, Vehicle vehicle) {
        String id = vehicle.getVid() + requestNumber;
        for(Token token : set) {
            if(token.getId().equals(id))
                  return token;
        }
        return null;
    }
}
