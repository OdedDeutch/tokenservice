/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.tokenServer;

import isr.commonTools.Service;
import datacore.Vehicle;
import isrdata.CCProcess;
import isrdata.IsrEvents;
import zlib.ZLog;
/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class TREvents extends IsrEvents {
    private final TokensHolder tokenHolder = TokensHolder.Init();
    private static final ZLog logger = TRMain.getLogger();
    
    public TREvents() { }
    
    @Override
    protected boolean onTokenRequest(Vehicle vehicle, String serviceID, String reqRefNum) {    
        String id = vehicle.getVid() + "-" + reqRefNum;
         CCProcess cc = (CCProcess)vehicle.getChild(CCProcess.ObjectCode, null);
        logEvent("REQUEST", id, "1", "Event fired. requested service: " + serviceID);
        String token = tokenHolder.getToken(reqRefNum, vehicle);
        logEvent("REQUEST", id, "1.1", "Token value - " + token);
        logEvent("REQUEST", id, "2", "Sending token to " + serviceID);
        boolean ret = cc.sendTokenToService(Service.CreditService.getName(), token, reqRefNum);
        cc.commit();
        Runnable r = new MyRunnable(id, reqRefNum, vehicle);
        new Thread(r).start();
        return true;
    }
    
    @Override
    protected boolean onTokenConfirm(Vehicle vehicle, String reqRefNum){
        String id = vehicle.getVid() + "-" + reqRefNum;
        CCProcess cc = (CCProcess)vehicle.getChild(CCProcess.ObjectCode, null);
        logEvent("CONFIRM", id, "1", "Event fired.");
        String token = tokenHolder.getToken(reqRefNum, vehicle);
        logEvent("CONFIRM", id, "2", "Got token.");
        boolean ret = cc.sendTokenToVehicle(Service.TokenService.getName(), token, reqRefNum);
        cc.commit();
        
        //this is for Haim - it should be the correct response to the vehicle, but need to fix it with Grar Yorma.
        boolean ret2 = cc.sendTokenToVehicle(Service.CreditService.getName(), token, reqRefNum);
        cc.commit();
        tokenHolder.remove(reqRefNum, vehicle);
        logEvent("CONFIRM", id, "3", "Send to vehicle.");
        return true;
    }
    
    public static void logEvent(String event, String requestId, String serial, String msg) {
        logger.logEvent(String.format("%10s id-%30s \tStep#%s-%s", event, requestId, serial, msg));
    }
    
    public static void logEvent(String event, String requestId, String serial, String msg, Exception ex) {
        logger.logEvent(String.format("%10s id-%30s \tStep#%s-%s", event, requestId, serial, msg));
        logger.logException(ex);
    }
 }
