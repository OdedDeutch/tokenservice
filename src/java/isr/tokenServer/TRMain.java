/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.tokenServer;

import isrdata.IsrEvents;
import isrdata.IsrMain;
import isrdata.IsrQueries;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class TRMain extends IsrMain {

    private static final String APP_NAME = "TN";
    private static final String VERSION = "O1.3"; // ver O1.3 - implementation of datacore Z4.03
    private static final String ROOT_DIRECTORY = "/isr/IsrTokenServer/";

    
    public TRMain() {
        super(ROOT_DIRECTORY, APP_NAME, VERSION);
    }

    @Override
    protected IsrEvents createEventHandler() {
        return new TREvents();
    }
    
    public static TRMain getInstance() {
        return (TRMain)IsrMain.getInstance();
    }

    @Override
    public boolean init() {
        try {
            if (!super.init())
                return false;
            
            return IsrQueries.createChargeService(APP_NAME, null);
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    protected String[] getProducedObjects() {
        return new String[] {};
    }

    @Override
    protected String[] getConsumedObjects() {
        return new String[] {};
    }
}
