/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.tokenServer;

import isr.commonTools.Service;
import datacore.Vehicle;
import isrdata.CCProcess;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class MyRunnable implements Runnable{
    String id;
    String reqRefNum;
    Vehicle vehicle;
    
    public MyRunnable(String id, String reqRefNum, Vehicle vehicle) {
        this.id = id;
        this.reqRefNum = reqRefNum;
        this.vehicle = vehicle;
    }

    @Override
    public void run() {
        TokensHolder tokenHolder = TokensHolder.Init();
        CCProcess cc = (CCProcess)vehicle.getChild(CCProcess.ObjectCode, null);
        try {
            Thread.sleep(10000);
            TREvents.logEvent("REQUEST", id, "3", "Cheking token existence.");
            if(tokenHolder.isTokenExist(reqRefNum, vehicle)) {
                TREvents.logEvent("REQUEST", id, "4", "Token exist, sending an error.");
                cc.sendTokenToVehicle(Service.CreditService.getName(), "CrErrMN", reqRefNum);
                cc.commit();
            } else {
                TREvents.logEvent("REQUEST", id, "4", "No token found. Doing nothing.");
            }
        } catch (InterruptedException ex) {
            TREvents.logEvent("REQUEST", id, "5", "Exception during token check.", ex);
        }
        
    }
    
}
